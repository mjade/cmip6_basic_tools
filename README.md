A collection of lower level scripts that use the system cdo to collect and
organise CMIP6 data from the IAC CMIP6 archive. The paths are hard-coded to our
institute's archive but can be easily adapted to any archive that follows the
ESGF structure.

The scripts allow users to get the data per variable, experiment, model either
as a netCDF4 file or directly loaded as a numpy array.

Additional cdo operations like computing annual means, remapping to a grid obtained from another model (or any other) can be included as data pre-processing.

# CMIP6 Beginner's Guide

The Coupled Model Intercomparison Project (CMIP) data can be found on `/net/atmos/data/`, where you can choose between different CMIPs - `cmip5/`, `cmip6/` for the _raw data_. The is also a folder with _pre-processed data_, interpolated into a 2.5 deg regular grid: `cmip6-ng/`, more information on the Next Generation (ng) processing can be found on https://zenodo.org/record/3734128 .

This documentation focuses on the organization of _raw data_. A typical path to the netCDF4 files in the following:

`/net/atmos/data/<model generation>/<experiment>/<model type>/<variable name>/<model name>/<ensemble memeber>/<g?>/*.nc`

`<model generation>` - can be either cmip5 or cmip6;

`<experiment>` - The core CMIP6 experiments (also known as DECK experiments) are piControl (control run), historical (1850-2015), 1pctCO2 (future scenario), abrupt-4xCO2 (future scenario). More information on each experiment and sub-experiment can be found at https://wcrp-cmip.github.io/CMIP6_CVs/docs/CMIP6_experiment_id.html. The piControl (pre-industrial control) experiments are the unforced control runs at pre-industrial conditions with a minimum length of ~500 years for CMIP6, they provide the background noise level for the other experiments and are useful for studying the internal variability of the system.

`<model type>` - The ESM models in CMIP6 are composed of coupled atmospheric, aerosol, atmospheric chemistry, land, land ice, ocean, ocean biogeochemistry, and sea ice models, some of which are of course optional and many institution do not couple all the components. A=atmosphere, AER=aerosols, O=ocean and so on, data is mainly monthly means (mon), but a few variables come in daily values (day), hence the categories 'AERday', 'Amon', 'Omon', etc. The file `ESM_models_sources` provides a list of the type of models used and their names for each participant in CMIP6. Most atmospheric variables that we use are under 'Amon'.

`<variable name>` - the variable name. A list of the acronyms and description of each variable, as well us under which model type it is found is in the file `variables_all`.

`<model name>` - all models, participating in the comparison project, which have submitted that variable

`<ensemble memeber>` - the model ensebles. They go like this: r1i1p1f1 (r=realization, i=initialization, p=physics, f=forcing), the number indicating which ones are different for the specific ensemble member.

`<g?>` - the grid. Usually data is given either on the native grid (`<gn>`) or is regridded (`<gr>`). In some cases, both are given.

The next parts of the path are the netCDF4 files themselves.


# How to handle the data?

Basically, if you just need to look into the variable statistic, the best way to do your analysis is in the basic model grid, i.e. without interpolation. If you want multimodel statistics however, you need to choose your interpolation mapping according to the variable. I use second order conservative remapping `cdo -remapcon2` for the radiation fluxes. Note that models don't agree that much of everything and it is good to always have the multimodel spread, together with the mean/median. The easiest way to combine the separate netCDF4 files and work with them is through `cdo mergetime`. The file `functions_raw_data.py` contains a class of functions, which should make it easy to loop through the models and ensemble members for a specific variable, interpolate, calculate yearly averages or deseasonalize the monthly means, if necessary. See example usage in examples/ and comments therein. 

