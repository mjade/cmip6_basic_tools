from subprocess import getoutput, run
from matplotlib import subprocess
from netCDF4 import Dataset
from cmip6_basic_tools.functions_raw_data import RawData, cbt
import datetime
import os


class OrgData:
    def __init__(self, working_dir='', variable_name=None, experiment='piControl', model_type='Amon', model_generation='cmip6', model_name='GFDL-ESM4', model_ensemble='r1i1p1f1'):
        self.variable_name = variable_name
        self.experiment = experiment
        self.model_type = model_type
        self.model_generation = model_generation
        self.model_name = model_name
        self.model_ensemble = model_ensemble
        self.working_dir = working_dir
        # for regridding if desired
        self.target_grid = '/net/atmos/data/cmip6/piControl/Amon/rlds/GFDL-ESM4/r1i1p1f1/gr1/rlds_Amon_GFDL-ESM4_piControl_r1i1p1f1_gr1_000101-010012.nc'
        self.year_range = None  # or (1920-2015)

        # Initialize temporary file names, useful for cdo.
        self.tmp_str = 'tmp_' + str(datetime.datetime.timestamp(datetime.datetime.now()))
        self.tmp_a = os.path.join(self.working_dir, self.tmp_str + '_a_outfile.nc')
        self.tmp_b = os.path.join(self.working_dir, self.tmp_str + '_b_outfile.nc')
        self.tmp_c = os.path.join(self.working_dir, self.tmp_str + '_c_outfile.nc')

    # def __del__(self):  # Destructor that deletes tmp files; automatically called on garbage collection
    def destructor(self):  # I call it manually because otherwise the pointer return doesn't work.
        if os.path.isfile(self.tmp_a): os.remove(self.tmp_a)
        if os.path.isfile(self.tmp_b): os.remove(self.tmp_b)
        if os.path.isfile(self.tmp_c): os.remove(self.tmp_c)

    def get_timeseries(self, variable_name=None, model_type=None, lat_lon=None, idx_lat_lon=None, annual_means=True, deseasonalise=False, return_pointer=False, regrid=False, extra_cdo=None):
        """get_timeseries.

            Returns time, var_data
            The time array in integers for annual_means and in datetime.datetime object for deseasonalised data.
            The var_data is 1D if either lat_lon or idx_lat_lon is specified, and 3D if they aren't specified.

        :param self:
        :param variable_name:
        :param model_type:
        :float tuple lat_lon:
        :int tuple idx_lat_lon:
        :bool annual_means:
        :bool deseasonalise:
        """
        # changes the class object
        if variable_name is not None:
            self.variable_name = variable_name
        if model_type is not None:
            self.model_type = model_type
        if self.year_range is not None:
            year_subset = True
        else:
            year_subset = False

        var_pi = RawData(variable_name=self.variable_name,
                         experiment=self.experiment, model_type=self.model_type, model_generation=self.model_generation, working_dir=self.working_dir)

        if regrid is True:
            var_pi.declare_target_grid(self.target_grid)
        tmp_outfile = var_pi.handle_file_list(self.model_name, self.model_ensemble, annual_means=annual_means, deseasonalise=deseasonalise, regrid=regrid, extra_cdo=extra_cdo)

        if year_subset:
            out_st = subprocess.run(['cdo', '-O', '-selyear,' + str(self.year_range[0]) + '/' + str(self.year_range[1]), tmp_outfile, self.tmp_a], cwd=self.working_dir, capture_output=True)
            print(out_st.stderr)

        if return_pointer:
            print('Temporary files are not automatically deleted. Delete the pointer yourself.')
            if year_subset:
                return self.tmp_a
            else:
                return tmp_outfile

        fh = Dataset(tmp_outfile, mode='r')
        time = cbt.get_years(fh, return_full_dates=deseasonalise)  # return full dates if deseasonalise is true
        if lat_lon is not None:
            grid_lat = fh.variables['lat'][:]
            grid_lon = fh.variables['lon'][:]
            idx_lat, idx_lon = cbt.ll_to_xy(lat=lat_lon[0], lon=lat_lon[1], grid_lat=grid_lat, grid_lon=grid_lon)
            var_data = fh.variables[self.variable_name][:, idx_lat, idx_lon]  # t, lat, lon
        elif idx_lat_lon is not None:
            idx_lat, idx_lon = idx_lat_lon[0], idx_lat_lon[1]
            var_data = fh.variables[self.variable_name][:, idx_lat, idx_lon]  # t, lat, lon
        else:
            var_data = fh.variables[self.variable_name][:, :, :]  # t, lat, lon; Note: for ICON, just use return_pointer=True
            self.lons = fh.variables['lon'][:]
            self.lats = fh.variables['lat'][:]
            self.lons_b = fh.variables['lon_bnds'][:]
            self.lats_b = fh.variables['lat_bnds'][:]
        fh.close()

        if year_subset:
            idx = (time >= self.year_range[0]) & (time <= self.year_range[1])
            time = time[idx]
            var_data = var_data[idx]

        var_pi.destructor()  # deletes tmp files
        return time, var_data

    def get_timeseries_region(self, variable_name=None, model_type=None, region=(5, 30, 44, 55), plevel=None, annual_means=True, deseasonalise=False, return_pointer=False, extra_cdo=None):
        """get_timeseries_region.

            Returns time, var_data
            The time array in integers for annual_means and in datetime.datetime object for deseasonalised data.
            The var_data is 1D, for the region specified by the bounding box.

        :param self:
        :param variable_name:
        :param model_type:
        :param region: default is Central Europe. (lon1, lon2, lat1, lat2)
        :bool annual_means:
        :bool deseasonalise:
        :param extra_cdo: Extra cdo operators to chain. Default None. An array of strings to additionally pass to cdo, for example  extra_cdo=['-sellevel,50000']
        """

        # changes the class object
        if variable_name is not None:
            self.variable_name = variable_name
        if model_type is not None:
            self.model_type = model_type

        var_pi = RawData(variable_name=self.variable_name,
                         experiment=self.experiment, model_type=self.model_type, model_generation=self.model_generation, working_dir=self.working_dir)

        tmp_outfile = var_pi.handle_file_list(self.model_name, self.model_ensemble, annual_means=annual_means, deseasonalise=deseasonalise, regrid=False, extra_cdo=extra_cdo)

        tmp_outfile2 = self.tmp_c
        region_str = str(region[0]) + ',' + str(region[1]) + ',' + str(region[2]) + ',' + str(region[3])
        out_st = subprocess.run(['cdo', '-fldmean', '-sellonlatbox,' + region_str, tmp_outfile, tmp_outfile2], cwd=self.working_dir, capture_output=True)
        print(out_st.stderr)

        if return_pointer:
            return tmp_outfile

        fh = Dataset(tmp_outfile2, mode='r')
        var_data = fh.variables[self.variable_name][:, 0, 0]
        time = cbt.get_years(fh, return_full_dates=deseasonalise)  # return full dates if deseasonalise is true
        fh.close()
        return time, var_data

    def get_ci_timeseries(self, ci='pdo_timeseries_mon', annual_means=True, deseasonalise=False):
        """get_CI_timeseries.
            Works only for piControl and historical, because I have the
            climate indices calculated for them.

        :param self:
        :param ci: can be 'amo_timeseries_mon', 'amoc_timeseries_ann', 'ipo_timeseries_mon', 'nam_timeseries_ann',
                          'indian_ocean_dipole', 'nao_timeseries_ann', 'nino34', 'nino4', 'north_tropical_atlantic',
                          'npo_timeseries_ann', 'pdo_timeseries_mon', 'pna_timeseries_ann', 'sam_timeseries_ann',
                          'south_tropical_atlantic', 'southern_ocean', 'sst_global_avg_ann'

        :bool annual_means: default is True
        :bool deseasonalise: default is False
        """

        # only works for piControl and historical
        path_to_index = '/net/o3/echam/bchtirkova/cvdp_output/downloaded/' + self.experiment + '/'
        if self.model_ensemble == '':  # to work with reanalyses
            cvdp_file = getoutput('ls ' + path_to_index + self.model_name + '.cvdp_data' + '*.nc')
        else:
            cvdp_file = getoutput('ls ' + path_to_index + self.model_name + '_' + self.model_ensemble + '*.nc')

        if not os.path.isfile(cvdp_file):
            print(self.model_name, self.model_ensemble, 'does not have a cvdp file. Will result in an ERROR')

        if annual_means is False and deseasonalise is False:
            file_to_open = cvdp_file
        if '_ann' in ci:
            file_to_open = cvdp_file
        else:
            if deseasonalise:
                out_st00 = run(['cdo', '-O', '-ymonmean', '-selvar,' + ci, cvdp_file, self.tmp_a], cwd=self.working_dir, capture_output=True)
                out_st01 = run(['cdo', '-O', '-sub', '-selvar,' + ci, cvdp_file, self.tmp_a, self.tmp_b], cwd=self.working_dir, capture_output=True)
                # print(out_st00.stderr, out_st01.stderr)  # prints out errors from other variables
                file_to_open = self.tmp_b
            if annual_means:
                out_st00 = run(['cdo', '-O', '-yearmonmean', '-selvar,' + ci, cvdp_file, self.tmp_b], cwd=self.working_dir, capture_output=True)
                # print(out_st00.stderr)
                file_to_open = self.tmp_b

        fh_cvdp = Dataset(file_to_open, mode='r')
        ci_ts = fh_cvdp.variables[ci][:]
        # couldn't get time units to work, workaround with cdo (see below)
        fh_cvdp.close()

        # would probably only work with annual means
        # years are essential because cvdp sometimes misses the first/last time step
        years = getoutput('cdo -s -w -showyear ' + file_to_open).split()  # info from infile using cdo
        years_int = [int(year) for year in years]

        return years_int, ci_ts
