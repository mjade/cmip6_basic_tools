from subprocess import getoutput, run
import os
from netCDF4 import Dataset
import numpy as np
import datetime


class RawData:
    def __init__(self, working_dir='',
                 variable_name='rsds', experiment='piControl', model_type='Amon', model_generation='cmip6'):
        self.variable_name = variable_name
        self.experiment = experiment
        self.model_type = model_type
        self.model_generation = model_generation
        self.working_dir = working_dir
        self.model_name = ''
        self.model_ensemble = ''

        # Initialize temporary file names, useful for cdo.
        self.tmp_str = 'tmp_' + str(datetime.datetime.timestamp(datetime.datetime.now()))
        self.tmp_a = os.path.join(self.working_dir, self.tmp_str + '_a_outfile.nc')
        self.tmp_b = os.path.join(self.working_dir, self.tmp_str + '_b_outfile.nc')
        self.tmp_c = os.path.join(self.working_dir, self.tmp_str + '_c_outfile.nc')
        self.tmp_cdo_a = os.path.join(self.working_dir, self.tmp_str + '_cdo_a_outfile.nc')
        self.tmp_cdo_b = os.path.join(self.working_dir, self.tmp_str + '_cdo_b_outfile.nc')
        self.cdo_status = 'ok'  # The last cdo command status is passed here. For debugging.

        if not os.path.isdir(working_dir):
            run(['mkdir', working_dir])

    # def __del__(self):   # Destructor that deletes tmp files; automatically called on garbage collection
    def destructor(self):  # I call it manually because otherwise the pointer return doesn't work.
        if os.path.isfile(self.tmp_a): os.remove(self.tmp_a)
        if os.path.isfile(self.tmp_b): os.remove(self.tmp_b)
        if os.path.isfile(self.tmp_c): os.remove(self.tmp_c)
        if os.path.isfile(self.tmp_cdo_a): os.remove(self.tmp_cdo_a)
        if os.path.isfile(self.tmp_cdo_b): os.remove(self.tmp_cdo_b)

    def get_model_names(self):
        data_dir = '/net/atmos/data/' + self.model_generation + '/' + self.experiment + '/' + self.model_type +\
                   '/' + self.variable_name
        model_names = getoutput('ls ' + data_dir).split('\n')
        # Note: rewrite this like a human, using os.listdir and os.path.join
        return model_names

    def get_ensemble_members(self, model_name):
        data_dir = '/net/atmos/data/' + self.model_generation + '/' + self.experiment + '/' + self.model_type + '/'\
                   + self.variable_name + '/' + model_name + '/'
        model_ensembles = getoutput('ls ' + data_dir).split('\n')
        return model_ensembles

    def data_filter(self, file_list):
        # Get rid of erroneous files, I occasionally email IAC_CMIP6_ARCHIVE <cmip6-archive@env.ethz.ch> and
        # they remove them.
        new_list = []
        for file in file_list:
            if self.model_generation == 'cmip6' and self.experiment == 'piControl' and self.model_type == 'Amon':
                if 'ACCESS-CM2' in file and '095001-144912.nc' not in file:
                    print('Excluding:', file)
                    continue
                if 'CAS-ESM2-0' in file and '000101-054912.nc' not in file:
                    print('Excluding:', file)
                    continue
                if 'FIO-ESM-2-0' in file and '080101-087512.nc' in file:
                    print('Excluding:', file)
                    continue
            new_list.append(file)
        return new_list

    def get_netcdf_file_list(self, model_name, model_ensemble):
        data_dir = '/net/atmos/data/' + self.model_generation + '/' + self.experiment + '/' + self.model_type +\
                   '/' + self.variable_name + '/' + model_name + '/' + model_ensemble + '/'
        gn_or_gr = getoutput('ls ' + data_dir).split('\n')  # should be the same for both
        gn_or_gr_ind = 0
        if 'gn' in gn_or_gr:  # preferentially take gn (native grid)
            gn_or_gr_ind = gn_or_gr.index('gn')
        if os.path.isdir(data_dir + gn_or_gr[gn_or_gr_ind]):
            data_dir = data_dir + gn_or_gr[gn_or_gr_ind] + '/'
        netcdf_file_list_nofilter = getoutput('ls ' + data_dir + '*').split('\n')
        netcdf_file_list = self.data_filter(netcdf_file_list_nofilter)
        self.last_path = data_dir
        return netcdf_file_list

    def declare_target_grid(self, target_grid_file='/net/atmos/data/cmip6/piControl/Amon/rsds/'
                                                   'CNRM-CM6-1-HR/r1i1p1f2/gr/rsds_Amon_CNRM-CM6-1-'
                                                   'HR_piControl_r1i1p1f2_gr_185001-214912.nc'):
        # give either netcdf of cdo grid description txt file
        if target_grid_file[-3:] == '.nc':
            # model file, in whose grid to interpolate; default is CNRM-HR as the finest grid.
            self.grid_file = self.working_dir + 'gridfile.txt'
            getoutput('cdo -griddes ' + target_grid_file + ' > ' + self.grid_file)
            self.n_lons, self.n_lats = cbt.get_xy_dims(target_grid_file)
            # print(self.n_lats, self.n_lons)

        elif target_grid_file[-3:] == 'txt':
            self.grid_file = target_grid_file

    def declare_target_point(self, target_lat, target_lon):
        # generates a grid file with 1 lat lon point
        self.grid_file = self.working_dir + 'gridfile.txt'
        self.n_lons, self.nlats = 1, 1  # set number of lon and lat points
        with open(self.grid_file, 'w') as grdfile:
            print('gridtype  : latlon',
                  'gridsize  : 1',
                  'xname     : lon',
                  'xlongname : longitude',
                  'xunits    : degrees_east',
                  'yname     : lat',
                  'ylongname : latitude',
                  'yunits    : degrees_north',
                  'xsize     : 1',
                  'ysize     : 1',
                  'xvals     : ' + str(target_lon),
                  'yvals     : ' + str(target_lat),
                  'xbounds   : ' + str(target_lon) + ', ' + str(target_lon),
                  'ybounds   : ' + str(target_lat) + ', ' + str(target_lat),
                  sep='\n', file=grdfile)
            grdfile.close()

    def handle_file_list(self, model_name=None, model_ensemble=None, annual_means=True, deseasonalise=False, regrid=False, extra_cdo=None):
        """
        Expected error message after update:
        File "/home/bchtirkova/Projects/cmip6_basic_tools/cmip6_basic_tools/functions_raw_data.py", line 64, in get_netcdf_file_list
    data_dir = '/net/atmos/data/' + self.model_generation + '/' + self.experiment + '/' + self.model_type +\
TypeError: can only concatenate str (not "list") to str
        
        :param extra_cdo: Extra cdo operators to chain.
        """
        if model_name is None:
            model_name = self.model_name
        if model_ensemble is None:
            model_ensemble = self.model_ensemble

        netcdf_file_list = self.get_netcdf_file_list(model_name, model_ensemble)

        # mergetime, annual means or desesoanalized data and remap with cdo -
        # one command because of cdo multiprocessing efficiency
        cdo_command = ['cdo', '-O', '-s']
        trim_first, trim_last = False, False

        if regrid:
            if model_name == 'ICON-ESM-LR':
                cdo_command.append('-remapcon,' + self.grid_file)  # first order conservative remapping especially for ICON
            else:
                cdo_command.append('-remapcon2,' + self.grid_file)
        if annual_means:
            cdo_command.append('-yearmonmean')
        if extra_cdo is not None:
            for extra_command in extra_cdo:
                cdo_command.append(extra_command)

        if not os.path.isfile(netcdf_file_list[0]):
            print(netcdf_file_list[0])
            print('This model does not have this experiment')
            exit()

        out_st_month1 = run(['cdo', '-showmon', '-seltimestep,1', netcdf_file_list[0]], capture_output=True)
        out_st_month12 = run(['cdo', '-showmon', '-seltimestep,-1', netcdf_file_list[-1]], capture_output=True)
        # print(out_st_month1)
        # print(out_st_month12)

        if int(out_st_month1.stdout) != 1:
            trim_first = True
            print('warning! incomplete years in model ', netcdf_file_list[0], ' will be trimmed')
        if int(out_st_month12.stdout) != 12:
            trim_last = True
            print('warning! incomplete years in model ', netcdf_file_list[-1], ' will be trimmed')

        cdo_command.extend(['-mergetime', *netcdf_file_list, self.tmp_a])
        self.cdo_status = run(cdo_command, cwd=self.working_dir, capture_output=True)

        # Exit if cdo produces an error.
        if 'Abort' in str(self.cdo_status):
            print(self.cdo_status)
            print('Error with cdo. Check cdo error message above. Exiting!')
            exit()

        if deseasonalise:
            out_st00 = run(['cdo', '-O', '-ymonmean', self.tmp_a, self.tmp_b], cwd=self.working_dir, capture_output=True)
            out_st01 = run(['cdo', '-O', '-sub', self.tmp_a, self.tmp_b, self.tmp_c], cwd=self.working_dir, capture_output=True)
            print(out_st00.stderr, out_st01.stderr)
            return self.tmp_c

        # If years are incomplete (missing months), cut first and last year to avoid cdo calculating yearmonmean over incomplete years.
        if annual_means:
            # faster with one command than calling cdo individually
            if trim_first and not trim_last:
                out_st00 = run(['cdo', '-O', '-delete,timestep=1', self.tmp_a, self.tmp_c], cwd=self.working_dir, capture_output=True)
                return self.tmp_c
            elif trim_last and not trim_first:
                out_st00 = run(['cdo', '-O', '-delete,timestep=-1', self.tmp_a, self.tmp_c], cwd=self.working_dir, capture_output=True)
                return self.tmp_c
            elif trim_last and trim_first:
                out_st00 = run(['cdo', '-O', '-delete,timestep=1,-1', self.tmp_a, self.tmp_c], cwd=self.working_dir, capture_output=True)
                return self.tmp_c

        return self.tmp_a

    def years_simulation(self, model_name, model_ensemble):
        netcdf_file_list = self.get_netcdf_file_list(model_name, model_ensemble)
        path = self.last_path

        files = getoutput('ls ' + path).split('\n')
        years_arr = []
        for file in files:
            out_st = run(['cdo', '-s', '-nyear', file], cwd=path, capture_output=True)
            num_years = int(out_st.stdout)
            years_arr.append(num_years)
        return sum(years_arr)

    def institution_info(self, infile):
        institution_id, institution_full_name = '', ''
        fh = Dataset(infile, mode='r')
        if self.model_generation == 'cmip5':
            institution_id = fh.getncattr('institute_id')
            institution_full_name = fh.getncattr('institution')
        if self.model_generation == 'cmip6':
            institution_id = fh.getncattr('institution_id')
            institution_full_name = fh.getncattr('institution')
        fh.close()
        return institution_id, institution_full_name


class cbt:  # cmip6 basic tools. Can do a better job organising here

    def get_models_cross_section(variable_class_list):
        """get_models_cross_section.

        Returns model_ensemble_dict={model_name: [ensembles_list]}
        Find all models and their ensemble members that provide the listed variables.
        Can be also used with only one variable to get a dictionary of all models and their ensebles.

        :param variable_class_list: A list of RawData objects
        """
        model_ensemble_dict = {}
        model_names_all = []
        for variable_class in variable_class_list:
            mn = variable_class.get_model_names()
            model_names_all.append(set(mn))  # convert list to set
        model_intersection_set = set.intersection(*model_names_all)
        for model_name in sorted(model_intersection_set):
            model_ensembles_all = []
            for variable_class in variable_class_list:
                es = variable_class.get_ensemble_members(model_name)
                model_ensembles_all.append(set(es))  # convert list to set
            ensemble_intersection_set = set.intersection(*model_ensembles_all)
            model_ensemble_dict.update({model_name: list(sorted(ensemble_intersection_set))})
        return model_ensemble_dict

    def all_trends_data(infile, trend_length, working_dir):
        """all_trends_data.
            Computes trends from file pointer (netCDF4 file) using cdo -trend.
            Faster than scipy but does not compute p, r and se.
        :param infile: Pointer to netCDF4 file.
        :param trend_length: Trend length in years.
        :param working_dir: Where to put the temporary files.
        """
        import os
        tmp_str = 'tmp_' + str(datetime.datetime.timestamp(datetime.datetime.now()))
        tmp_cdo_a = os.path.join(working_dir, tmp_str + '_cdo_a_outfile.nc')
        tmp_cdo_b = os.path.join(working_dir, tmp_str + '_cdo_b_outfile.nc')

        years = getoutput('cdo -s -showyear ' + infile).split()  # info from infile using cdo
        n_years = len(years)
        start_year0, end_year0 = int(years[0]), int(years[-1])
        n_trends = n_years - trend_length + 1  # might need to revert to +2
        n_lons, n_lats = cbt.get_xy_dims(infile)
        model_array_trend = np.empty(shape=(n_trends, n_lats, n_lons))
        variable_name = getoutput('cdo -s -showname ' + infile).strip()
        print(infile)
        print(start_year0, end_year0, n_years, n_lons, n_lats)
        for trend_range in range(0, n_trends):
            start_year = start_year0 + trend_range
            end_year = start_year0 + trend_range + trend_length - 1  # -1 because cdo is inclusive
            out_st = run(['cdo', '-O', '-trend', '-selyear,' + str(start_year) + '/' + str(end_year),
                          infile, tmp_cdo_a, tmp_cdo_b], cwd=working_dir, capture_output=True)
            if 'Abort' in str(out_st.stderr):
                print('9999 - cdo did not calculate trend')
                print(out_st)
                continue
            # In cdo the parameter <b> is the actual trend in Wm-2/year
            fh = Dataset(tmp_cdo_b, mode='r')
            tr_b = fh.variables[variable_name][0, :, :]
            fh.close()
            model_array_trend[trend_range, :, :] = tr_b

        if os.path.isfile(tmp_cdo_a): os.remove(tmp_cdo_a)
        if os.path.isfile(tmp_cdo_b): os.remove(tmp_cdo_b)
        return model_array_trend

    def all_trends_data_scipy(infile, trend_length):
        # would only work with annual means
        from scipy.stats import linregress
        variable_name = getoutput('cdo -s -showname ' + infile).strip()
        fh = Dataset(infile, mode='r')
        var_data = fh.variables[variable_name][:, :, :]
        years = cbt.get_years(fh)
        n_years = len(years)
        # start_year0, end_year0 = int(years[0]), int(years[-1])
        n_trends = n_years - trend_length + 1  # might need to revert to +2
        n_lons, n_lats = cbt.get_xy_dims(infile)
        model_array_trend = np.empty(shape=(n_trends, n_lats, n_lons))
        model_array_r = np.empty(shape=(n_trends, n_lats, n_lons))
        model_array_p = np.empty(shape=(n_trends, n_lats, n_lons))
        model_array_se = np.empty(shape=(n_trends, n_lats, n_lons))
        n_lats = var_data.shape[1]
        n_lons = var_data.shape[2]
        fh.close()
        for trend_range in range(0, n_trends):
            start_year = trend_range
            end_year = trend_range + trend_length
            for grid_box_x in range(0, n_lons):
                for grid_box_y in range(0, n_lats):
                    y = var_data[start_year: end_year, grid_box_y, grid_box_x]
                    x = years[start_year: end_year]
                    linreg_res = linregress(x, y)
                    # slope, intercept, r, p, se = linregress(x, y)

                    model_array_trend[trend_range, grid_box_y, grid_box_x] = linreg_res.slope
                    model_array_r[trend_range, grid_box_y, grid_box_x] = linreg_res.rvalue
                    model_array_p[trend_range, grid_box_y, grid_box_x] = linreg_res.pvalue
                    model_array_se[trend_range, grid_box_y, grid_box_x] = linreg_res.stderr
        return model_array_trend, model_array_r, model_array_p, model_array_se

    def all_trends_data_scipy_1D(inarray, trend_length, return_stats=False):
        """all_trends_data_scipy_1D. Previously named all_trends_data_numpy.
            
            If return_stats=True, returns: slope, r, p, se.

            For 1D things.
            Doesn't use numpy but scipy.stats.linregress. See comment in code.

        :param inarray:  A 1D array.
        :int trend_length:  
        """
        # from numpy.polynomial import Polynomial
        from scipy.stats import linregress
        n_years = len(inarray)
        n_trends = n_years - trend_length + 1
        model_array_trend = np.empty(shape=(n_trends))
        model_array_r = np.empty(shape=(n_trends))
        model_array_p = np.empty(shape=(n_trends))
        model_array_se = np.empty(shape=(n_trends))
        x = np.arange(trend_length)
        for trend_range in range(0, n_trends):
            start_year = trend_range
            end_year = trend_range + trend_length
            y = inarray[start_year:end_year]
            # z, _ = Polynomial.fit(x, y, 1)
            # z = np.polyfit(x, y, 1)[0]
            """ Important: np.polyfit gives ~ the same results as cdo trend
            and scipy.stats.linregress. Polynomial.fit is supposed to be equivalent,
            but gives shit, hence I decided to use scipy.
            Can be also made to return r, p, se.
            """
            linreg_res = linregress(x, y)
            model_array_trend[trend_range] = linreg_res.slope
            model_array_r[trend_range] = linreg_res.rvalue
            model_array_p[trend_range] = linreg_res.pvalue
            model_array_se[trend_range] = linreg_res.stderr
        if return_stats:
            return model_array_trend, model_array_r, model_array_p, model_array_se
        else:
            return model_array_trend

    def ll_to_xy(lat, lon, grid_lat, grid_lon, unstructured=False):
        lat_diff = np.abs(lat - grid_lat)
        lon_diff = np.abs(lon - grid_lon)
        if unstructured == True:
            sum_diff = lat_diff + lon_diff
            idx_lat_lon = np.argmin(sum_diff)
            return idx_lat_lon, idx_lat_lon
        idx_lat = np.argmin(lat_diff)
        idx_lon = np.argmin(lon_diff)
        return idx_lat, idx_lon

    def get_xy_dims(infile):
        """get_xy_dims.

        Returns x_size, y_size (lon_size, lats_size)
        :param infile:
        """
        fh = Dataset(infile, mode='r')
        x_size = fh.dimensions['lon'].size
        y_size = fh.dimensions['lat'].size
        fh.close()
        return x_size, y_size

    def grid_increment(infile, metric='deg'):
        """grid_increment.

        :param infile:
        :param metric: Can be 'deg' or 'km'.
        """
        fh = Dataset(infile, mode='r')
        x_size = fh.dimensions['lon'].size
        fh.close()
        res = 360 / x_size
        if metric == 'km':
            res = res * 111
            res = np.around(res, 0)
        if metric == 'deg':
            res = np.around(res, 1)
        return res  # approximate resolution

    def get_lons_lats(infile):
        fh = Dataset(infile, mode='r')
        lons = fh.variables['lon'][:]
        lats = fh.variables['lat'][:]
        fh.close()
        return lons, lats

    def datetime_from_julday(julday_h, return_int_year=False):
        """datetime_from_julday.
        Don't use this function! Keep for reference.

        Returns the dates according to CMIP6 conventions (adds days to 1850-1-1).
        By default, returns a datetime.detetime object.

        :param julday_h:
        :param return_int_year: Can also return just the year as an integer.
        """

        from datetime import datetime, timedelta
        # for "days since 1850-01-01"
        res = [datetime(1850, 1, 1) + timedelta(days=jd) for jd in julday_h]
        if return_int_year:
            res = [int(whole_date.year) for whole_date in res]
        return res

    def get_years(fh, return_full_dates=False):
        """get_years.

        A generalized way to get the dates, requires the file handler as an extra argument.
        By default, returns just the integer years in a numpy array.

        :param fh: The netCDF4 file handler
        :param return_int_year: If True, returns a detetime.datetime object.
        """
        from netCDF4 import num2date
        # I'm not sure why I have these extra arguments. There was probably some trouble with the piControl date ranges.
        # Extra arguments are to return datetime instead of cftime. Though cftime is recommended.
        # res = num2date(fh.variables['time'][:], units=fh.variables['time'].units, only_use_cftime_datetimes=False, only_use_python_datetimes=True)
        res = num2date(fh.variables['time'][:], units=fh.variables['time'].units)

        if not return_full_dates:  # int years
            res = np.array([int(whole_date.year) for whole_date in res])
        return res

    def calculate_statistics(infile, variable_name):
        from numpy import mean, median, std
        from scipy.stats import variation, skew, kurtosis
        fh = Dataset(infile, mode='r')
        var_data = fh.variables[variable_name][:, :, :]  # t, lat, lon
        var_mean = mean(var_data, axis=0)
        var_median = median(var_data, axis=0)
        var_std = std(var_data, axis=0)
        var_cv = variation(var_data, axis=0)  # coefficient of variation
        var_skew = skew(var_data, axis=0)
        var_kurt = kurtosis(var_data, axis=0)
        lats = fh.variables['lat'][:]
        lons = fh.variables['lon'][:]
        fh.close()
        return_dict = {'lats': lats, 'lons':lons, 'mean': var_mean, 'median': var_median, 'std': var_std,
                       'cv': var_cv, 'skew': var_skew, 'kurt': var_kurt}
        return return_dict

    def get_percentiles(infile, variable_name):
        from numpy import percentile
        fh = Dataset(infile, mode='r')
        var_data = fh.variables[variable_name][:, :, :]  # t, lat, lon
        return_dict = {}
        for percentile_value in [0, 10, 25, 50, 75, 90, 100]:
            var_perc = percentile(var_data, q=percentile_value, axis=0)
            return_dict.update({percentile_value: var_perc})
        fh.close()
        return return_dict

    def get_percentiles_per_month(infile, variable_name, tmp_outfile):
        cbt()
        return_dict = {}
        for month in range(1, 13):
            cdo_command = ['cdo', 'selmon,' + str(month), infile, tmp_outfile]
            out_st = run(cdo_command, capture_output=True)
            print(out_st.stderr)
            percentiles = cbt.get_percentiles(tmp_outfile, variable_name)
            return_dict.update({month: percentiles})
        fh = Dataset(infile, mode='r')
        lats = fh.variables['lat'][:]
        lons = fh.variables['lon'][:]
        fh.close()
        return_dict.update({'lats': lats})
        return_dict.update({'lons': lons})
        return return_dict


    def get_correlation(infile1, infile2, variable_name_1, variable_name_2, autocorrelation=False, lag=1, coefficient='pearsonr'):
        # coefficient can be pearsonr or speramanr
        if coefficient == 'pearsonr':
            from scipy.stats import pearsonr as corrcoeff
        elif coefficient == 'spearmanr':
            from scipy.stats import spearmanr as corrcoeff

        fh1 = Dataset(infile1, mode='r')
        fh2 = Dataset(infile2, mode='r')
        var_data1 = fh1.variables[variable_name_1][:, :, :]
        var_data2 = fh2.variables[variable_name_2][:, :, :]
        if autocorrelation == True:
            var_data1 = var_data1[:lag*-1, :, :]  # try with a 12 month shift
            var_data2 = var_data2[lag:, :, :]
        fh1.close()
        fh2.close()

        corr_arr = np.full(shape=(2, var_data1.shape[1], var_data1.shape[2]), fill_value=999.)  # the dot is important

        if var_data1.shape != var_data2.shape:
            print('Error 9999 data inconsistency!')
            print(var_data1.shape)
            print(var_data2.shape)

        for grid_box_x in range(0, var_data1.shape[2]):
            for grid_box_y in range(0, var_data1.shape[1]):
                cor = corrcoeff(var_data1[:, grid_box_y, grid_box_x], var_data2[:, grid_box_y, grid_box_x])
                corr_arr[:, grid_box_y, grid_box_x] = np.array(cor)
        return corr_arr  # returns corr_ar[0, :, :]=r and corr_ar[1, :, :]=two-tailed p-value 

    def normality_tests(infile, variable_name):
        from scipy import stats  # anderson, kstest, norm
        import numpy as np

        fh = Dataset(infile, mode='r')
        var_data = fh.variables[variable_name][:, :, :]  # t, lat, lon
        # for a lognormal distribution
        # var_data = np.log(var_data1)
        n_lats = var_data.shape[1]
        n_lons = var_data.shape[2]
        fh.close()

        rejection_mask_ad5 = np.zeros(shape=(n_lats, n_lons))
        rejection_mask_ad15 = np.zeros(shape=(n_lats, n_lons))
        rejection_mask_ks = np.zeros(shape=(n_lats, n_lons), dtype=float)
        rejection_mask_ksd = np.zeros(shape=(n_lats, n_lons), dtype=float)

        # k2, p = normaltest(model_array_trend) - if you want to try out another test
        rejected_ad, rejected_ks, total = 0, 0, 0
        for grid_box_x in range(0, n_lons):
            for grid_box_y in range(0, n_lats):
                ad_statistic, ad_crit_values, ad_sign_levels = stats.anderson(var_data[:, grid_box_y, grid_box_x],
                                                                        dist='norm')

                # something changed and I couldn't use the old method  for ks test from cmip6_internal_variability/v2_submitted/
                # I use the following: https://stackoverflow.com/questions/46678239/p-value-is-0-when-i-use-scipy-stats-kstest-for-large-dataset
                a = var_data[:, grid_box_y, grid_box_x]
                ks_statistic, ks_pvalue = stats.kstest(a, stats.norm(loc=np.mean(a), scale=np.std(a)).cdf)
                # ks_statistic, ks_pvalue = stats.kstest(a, stats.lognorm(s=np.std(a), loc=np.mean(a)).cdf) - doesn't work, take it's logarithm and use norm
                # ks_statistic, ks_pvalue = stats.kstest(a, 'norm') - this only tests whether the distribution is normal normal with mu=0, std=1
                # And I test whether the sample comes from a distribution with mean - the mean of the sample and std - the mean of the std
                # print(ks_pvalue)
                # print('lognorm')

                # for a significance level of 5%, we need to compare the ad_statistic with ad_crit_values[2]
                # rejection_mask[i,j] = 1 means the null hypothesis was rejected and the distribution is NOT normal
                if ad_statistic > ad_crit_values[2]:
                    rejection_mask_ad5[grid_box_y, grid_box_x] = 1
                    rejected_ad += 1

                if ad_statistic > ad_crit_values[0]:
                    rejection_mask_ad15[grid_box_y, grid_box_x] = 1

                if ks_pvalue < 0.05:
                    rejected_ks += 1

                rejection_mask_ks[grid_box_y, grid_box_x] = ks_pvalue  # just store all p_values and you can make a rejection mask for all of them which are below 0.05
                rejection_mask_ksd[grid_box_y, grid_box_x] = ks_statistic  # bonus
                total += 1

        # print((rejected_ad / total))
        # print((rejected_ks / total))
        return rejection_mask_ad5, rejection_mask_ad15, rejection_mask_ks, rejection_mask_ksd  #[lat, lon]

    def moving_average(ts, w=11):
        """moving_average.

        :param ts:  The 1D time series array.
        :param w:   The moving average window. Choose an odd number! Default is 11.
        """
        side_nans = np.full(shape=int((w - 1) / 2), fill_value=np.nan)
        new_arr = np.convolve(ts, np.ones(w), 'valid') / w
        out_arr = np.concatenate((side_nans, new_arr, side_nans), axis=0)
        return out_arr

    def moving_average_arr(ts, w=11):
        """moving_average_arr.

        :param ts:  The 3D time series array (t, x, y).
        :param w:   The moving average window. Choose an odd number! Default is 11.
        """
        ts = np.array(ts)
        out_arr = np.full_like(ts, fill_value=np.nan)
        side_length = int((w - 1) / 2)
        for i in range(ts.shape[1]):
            for j in range(ts.shape[2]):
                ij_arr = np.convolve(ts[:, i, j], np.ones(w), 'valid') / w
                out_arr[side_length:-side_length, i, j] = ij_arr
        return out_arr
