from cmip6_basic_tools.functions_raw_data import RawData, cbt


def main():
    working_dir = '...'  # The path to your working directory, you should have write access, so change this. This time you don't need to write anything there, but just in case.
    var_pi = RawData(working_dir=working_dir, variable_name='rsds',
                     experiment='piControl', model_type='day', model_generation='cmip6')

    model_names = var_pi.get_model_names()  # Get a list of model names that provide the vaiable
    print(model_names)
    for model_name in model_names:  # Loop over the model list
        model_ensembles = var_pi.get_ensemble_members(model_name)  # get a list of the ensemble members
        print(model_ensemble)
        for model_ensemble in model_ensembles:
            file_list = var_pi.get_netcdf_file_list(model_name, model_ensemble)
            print(file_list)  # the list of pointers you need
            exit()


if __name__ == '__main__':
    main()
