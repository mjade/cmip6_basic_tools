from cmip6_basic_tools.functions_org_data import OrgData
import numpy as np


od = OrgData(working_dir='...')  # Should have write access.
od.model_generation = 'cmip6'
od.experiment = 'piControl'
od.model_type = 'Amon'
od.model_name = 'GFDL-ESM4'
od.model_ensemble = 'r1i1p1f1'

# load rlds
time, var_rlds = od.get_timeseries(variable_name='rlds', annual_means=True)
# var_rlds(t, lat, lon)

# Latitudes and longitudes, you should access them after calling od.get_timeseries()
lats = od.lats
lons = od.lons

print(var_rlds)
# Load other variables. Interesting ones are prw (mass content of water vapour in kg m-2), clt (cloud cover fraction).
# time, var_something = od.get_timeseries(variable_name='...', annual_means=True)
time, var_rlds = od.get_timeseries(variable_name='rlds', annual_means=True)
# ...(t, lat, lon)

# get an aggregated timeseries for a region
lon1 = 5   # minimum longitude
lon2 = 30  # maximum longitude
lat1 = 44  # minimum latitude
lat2 = 55  # maximum latitude
time, var_rlds_agg = od.get_timeseries_region(variable_name='rlds', annual_means=True, region=(lon1, lon2, lat1, lat2))
# var_rlds_agg(t)

# get the NAM timeseries
time_nam, nam = od.get_ci_timeseries('nam_timeseries_ann', annual_means=True)
# nam(t)

# you can also try PDO
time_pdo, pdo = od.get_ci_timeseries('pdo_timeseries_mon', annual_means=True)
# pdo(t)
