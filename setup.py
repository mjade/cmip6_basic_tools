from setuptools import setup, find_packages

setup(
    name="cmip6_basic_tools",
    version="0.1.0",
    author="ETH Zurich",
    author_email="boriana.chtirkova@env.ethz.ch",
    description=(
        "Simple CMIP6 thinngs."
    ),
    # long_description=read_file("README.md"),
    # long_description_content_type="text/markdown",
    keywords="",
    url="https://gitlab.com/mjade/cmip6_basic_tools",
    license="",
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=[
        # 'matplotlib>=2.0.0',
        'netcdf4>=1.2.7',
        'numpy>=1.18.0',
        'scipy>=0.19.0',
        ],
)

